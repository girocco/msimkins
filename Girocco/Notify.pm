package Girocco::Notify;

use strict;
use warnings;

BEGIN {
	use Girocco::Config;

	use JSON;
	use LWP::UserAgent;

	use RPC::XML;
	use RPC::XML::Client;
}


# This Perl code creates json payload within post-receive hook.

sub json_commit {
	my ($proj, $commit) = @_;

	my @gcmd = ($Girocco::Config::git_bin, '--git-dir='.$proj->{path});
	my $fd;

	open $fd, '-|', @gcmd, 'log', '-1', '--pretty=format:%ae %an%n%ai%n%s%n%n%b', $commit
		or die "cannot do git log: $! $?";
	my @l = <$fd>;
	chomp @l;
	close $fd;
	my ($ae, $an) = ($l[0] =~ /^(.*?) (.*)$/);
	my ($ai) = $l[1];
	my $msg = join("\n", splice(@l, 2));
	# Up to three trailing newlines in case of no body.
	chomp $msg; chomp $msg; chomp $msg;

	my ($rf, $af, $mf) = ([], [], []);
	open $fd, '-|', @gcmd, 'diff-tree', '--name-status', '-r', "$commit^", $commit
		or die "cannot do git diff-tree: $! $?";
	while (<$fd>) {
		chomp;
		my ($s, $file) = split(/\t/, $_);
		if ($s eq 'M') {
			push @$mf, $file;
		} elsif ($s eq 'A') {
			push @$af, $file;
		} elsif ($s eq 'R') {
			push @$rf, $file;
		}
	}
	close $fd;

	return {
		"removed"   => $rf,
		"message"   => $msg,
		"added"     => $af,
		"timestamp" => $ai,
		"modified"  => $mf,
		"url"       => $Girocco::Config::gitweburl."/".$proj->{name}.".git/commit/".$commit,
		"author"    => { "name" => $an, "email" => $ae },
		"id"        => $commit
	}; 
}

sub json {
	my ($url, $proj, $user, $ref, $oldrev, $newrev) = @_;

	my $pusher = {};
	if ($user) {
		$pusher = { "name" => $user->{name} };
		if ($user->{name} ne 'mob') {
			$pusher->{"email"} = $user->{email};
		}
	}

	my $commits = [];

	foreach my $commit (get_commits($proj, $ref, $oldrev, $newrev)) {
		push @$commits, json_commit($proj, $commit);
	}

	# This is backwards-compatible with GitHub (except the GitHub-specific
	# full project name construction sometimes performed in clients)
	my $payload = encode_json {
		"before" => $oldrev,
		"after"  => $newrev,
		"ref"    => $ref,

		"repository" => {
			"name"  => $proj->{name},
			# Girocco extension: full_name is full project name,
			# equivalent to GitHub's "owner[name]/name".
			"full_name" => $proj->{name}.".git",

			"url"   => $Girocco::Config::gitweburl.'/'.$proj->{name}.".git",
			# Girocco extension: Pull URL.
			"pull_url" => $Girocco::Config::gitpullurl.'/'.$proj->{name}.".git",

			"owner" => { "name" => "", "email" => $proj->{email} }
		},

		# Girocco extension
		"pusher" => $pusher,

		"commits" => $commits
	};

	# print "$payload\n";
	my $ua = LWP::UserAgent->new;
	$ua->timeout(5);
	$ua->post($url, { payload => $payload });
}


sub cia_commit {
	my ($cianame, $proj, $branch, $commit) = @_;

	my @gcmd = ($Girocco::Config::git_bin, '--git-dir='.$proj->{path});
	my $fd;

	open $fd, '-|', @gcmd, 'log', '-1', '--pretty=format:%an <%ae>%n%at%n%s', $commit
		or die "cannot do git log: $! $?";
	my @l = <$fd>;
	chomp @l;
	close $fd;
	foreach (@l) { s/&/&amp;/g; s/</&lt;/g; s/>/&gt;/g; }
	my ($a, $at, $subj) = @l;

	my @f;
	open $fd, '-|', @gcmd, 'diff-tree', '--name-status', '-r', "$commit^", $commit
		or die "cannot do git diff-tree: $! $?";
	while (<$fd>) {
		chomp;
		s/&/&amp;/g; s/</&lt;/g; s/>/&gt;/g;
		my ($status, $file) = split(/\t/, $_);
		push @f, $file;
	}
	close $fd;

	my $rev = substr($commit, 0, 12);

	my $msg = <<EOT;
	<message>
		<generator>
			<name>Girocco::Notify</name>
			<version>1.0</version>
		</generator>
		<source>
			<project>$cianame</project>
EOT
	if ($branch ne 'master') { # XXX: Check HEAD instead
		$msg .= "<branch>$branch</branch>";
	}
	$msg .= "</source>\n";
	$msg .= "<timestamp>$at</timestamp>\n";
	$msg .= "<body><commit><author>$a</author><revision>$rev</revision>\n";
	$msg .= "<url>$Girocco::Config::gitweburl/$proj->{name}.git/commit/$commit</url>\n";
	$msg .= "<files>\n";
	foreach (@f) { $msg .= "<file>$_</file>\n"; }
	$msg .= "</files><log>$subj</log></commit></body></message>\n";

	# print "$msg\n";
	my $rpc_client = new RPC::XML::Client "http://cia.vc/RPC2";
	my $rpc_request = RPC::XML::request->new('hub.deliver', $msg);
	my $rpc_response = $rpc_client->send_request($rpc_request);
	ref $rpc_response or print STDERR "XML-RPC Error: $RPC::XML::ERROR\n";
}

sub cia {
	my ($cianame, $proj, $ref, $oldrev, $newrev) = @_;

	# CIA notifications for branches only
	my $branch = $ref;
	$branch =~ s#^refs/heads/## or return;

	foreach my $commit (get_commits($proj, $ref, $oldrev, $newrev)) {
		cia_commit($cianame, $proj, $branch, $commit);
	}
}


sub get_commits {
	my ($proj, $ref, $oldrev, $newrev) = @_;

	my @gcmd = ($Girocco::Config::git_bin, '--git-dir='.$proj->{path});
	my $fd;

	open $fd, '-|', @gcmd, 'for-each-ref', '--format=%(refname)', 'refs/heads/'
		or die "cannot do git for-each-ref: $! $?";
	my @refs = <$fd>;
	chomp @refs;
	@refs = grep { $_ ne $ref } @refs;
	close $fd;

	my @revlims;
	if (@refs) {
		open $fd, '-|', @gcmd, 'rev-parse', '--not', @refs
			or die "cannot do git rev-list for revlims: $! $?";
		@revlims = <$fd>;
		chomp @revlims;
		close $fd;
	}

	my $revspec = (($oldrev =~ /^0+$/) ? $newrev : "$oldrev..$newrev");
	open $fd, '-|', @gcmd, 'rev-list', @revlims, $revspec
		or die "cannot do git rev-list: $! $?";
	my @revs = <$fd>;
	chomp @revs;
	close $fd;

	return @revs;
}


sub ref_change {
	my ($proj, $user, $ref, $oldrev, $newrev) = @_;

	chdir($proj->{path});

	# First, possibly send out various mails
	if ($proj->{notifymail}) {
		my $sender;
		if ($user) {
			if ($user->{name} eq 'mob') {
				$sender = "The Mob User <$proj->{email}>";
			} else {
				$sender = "$user->{name} <$user->{email}>";
			}
		} else {
			$sender = "$proj->{name} <$proj->{email}>";
		}
		system($Girocco::Config::basedir.'/taskd/mail.sh',
			"$ref", "$oldrev", "$newrev", $proj->{name},
			$sender)
			and warn "mail.sh failed";
	}

	# Next, send JSON packet to given URL if enabled.
	if ($proj->{notifyjson}) {
		json($proj->{notifyjson}, $proj, $user, $ref, $oldrev, $newrev);
	}

	# Also send CIA notifications.
	if ($proj->{notifycia}) {
		cia($proj->{notifycia}, $proj, $ref, $oldrev, $newrev);
	}
}


1;
