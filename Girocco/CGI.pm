package Girocco::CGI;

use strict;
use warnings;

use Girocco::Config;

BEGIN {
	our $VERSION = '0.1';
	our @ISA = qw(Exporter);
	our @EXPORT = qw(html_esc);

	use CGI qw(:standard :escapeHTML -nosticky);
	use CGI::Util qw(unescape);
	use CGI::Carp qw(fatalsToBrowser);
}


sub new {
	my $class = shift;
	my ($heading, $section) = @_;
	my $gcgi = {};

	$section ||= 'administration';

	$gcgi->{cgi} = CGI->new;

	my $cgiurl = $gcgi->{cgi}->url(-absolute => 1);
	($gcgi->{srcname}) = ($cgiurl =~ m#^.*/\([a-zA-Z0-9_.\/-]+?\.cgi\)$#); #
	$gcgi->{srcname} = "cgi/".$gcgi->{srcname} if $gcgi->{srcname};

	print $gcgi->{cgi}->header(-type=>'text/html', -charset => 'utf-8');

	print <<EOT;
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">

<head>
<title>$Girocco::Config::name :: $heading</title>
<link rel="stylesheet" type="text/css" href="$Girocco::Config::gitwebfiles/gitweb.css"/>
<link rel="stylesheet" type="text/css" href="$Girocco::Config::gitwebfiles/girocco.css"/>
<link rel="shortcut icon" href="$Girocco::Config::gitwebfiles/git-favicon.png" type="image/png"/>
<script src="$Girocco::Config::gitwebfiles/mootools.js" type="text/javascript"></script>
<script src="$Girocco::Config::gitwebfiles/girocco.js" type="text/javascript"></script>
</head>

<body>

<div class="page_header">
<a href="http://git.or.cz/" title="Git homepage"><img src="$Girocco::Config::gitwebfiles/git-logo.png" width="72" height="27" alt="git" style="float:right; border-width:0px;"/></a>
<a href="$Girocco::Config::gitweburl">$Girocco::Config::name</a> / $section / $heading
</div>

EOT

	bless $gcgi, $class;
}

sub DESTROY {
	my $self = shift;
	if ($self->{srcname} and $Girocco::Config::giroccourl) {
		print <<EOT;
<div align="right">
<a href="$Girocco::Config::giroccourl?a=blob;f=$self->{srcname}">(view source)</a>
</div>
EOT
	}
	print <<EOT;
</body>
</html>
EOT
}

sub cgi {
	my $self = shift;
	$self->{cgi};
}

sub err {
	my $self = shift;
	print "<p style=\"color: red\">@_</p>\n";
	$self->{err}++;
}

sub err_check {
	my $self = shift;
	my $err = $self->{err};
	$err and print "<p style=\"font-weight: bold\">Operation aborted due to $err errors.</p>\n";
	$err;
}

sub wparam {
	my $self = shift;
	my ($param) = @_;
	my $val = $self->{cgi}->param($param);
	defined $val and $val =~ s/^\s*(.*?)\s*$/$1/;
	$val;
}

sub srcname {
	my $self = shift;
	my ($srcname) = @_;
	$self->{srcname} = $srcname if $srcname;
	$self->{srcname};
}

sub html_esc {
	my ($str) = @_;
	$str =~ s/&/&amp;/g;
	$str =~ s/</&lt;/g; $str =~ s/>/&gt;/g;
	$str =~ s/"/&quot;/g;
	$str;
}

sub print_form_fields {
	my $self = shift;
	my ($fieldmap, $valuemap, @fields) = @_;

	foreach my $field (map { $fieldmap->{$_} } @fields) {
		print '<tr><td class="formlabel">'.$field->[0].':</td><td>';
		if ($field->[2] eq 'text') {
			print '<input type="text" name="'.$field->[1].'" size="80"';
			print ' value="'.$valuemap->{$field->[1]}.'"' if $valuemap;
			print ' />';
		} else {
			print '<textarea name="'.$field->[1].'" rows="5" cols="80">';
			print $valuemap->{$field->[1]} if $valuemap;
			print '</textarea>';
		}
		print "</td></tr>\n";
	}
}


1;
