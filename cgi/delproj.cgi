#!/usr/bin/perl
# (c) Petr Baudis <pasky@suse.cz>
# GPLv2

use strict;
use warnings;

use lib ".";
use Girocco::CGI;
use Girocco::Config;
use Girocco::Project;
use Girocco::Util;

my $gcgi = Girocco::CGI->new('Project Removal');
my $cgi = $gcgi->cgi;

my $name = $cgi->param('name');

unless (defined $name) {
	print "<p>I need the project name as an argument now.</p>\n";
	exit;
}

if (!Girocco::Project::valid_name($name)) {
	print "<p>Invalid project name. Go away, sorcerer.</p>\n";
	exit;
}

if (!Girocco::Project::does_exist($name)) {
	print "<p>Sorry but this project does not exist. Now, how did you <em>get</em> here?!</p>\n";
	exit;
}

my $proj = Girocco::Project->load($name);
$proj or die "not found project $name, that's really weird!";
$proj->{cpwd} = $cgi->param('cpwd');

if (!$proj->{mirror}) {
	print "<p>Sorry but you can remove only mirrored projects. Pushed projects cannot be removed. Please tell the administrator if you really want to.</p>\n";
	exit;
}

if ($proj->has_forks()) {
	print "<p>Sorry but this project has forks associated. Such projects cannot be removed. Please tell the administrator if you really want to.</p>\n";
	exit;
}

if ($cgi->param('y0')) {
	# submitted
	if (not $proj->authenticate($gcgi)) {
		exit;
	}
	$proj->delete;
	print "<p>Project successfuly deleted. Have a nice day.</p>\n";
	exit;
}

my $url = $proj->{url};

print <<EOT;
<p>Please confirm that you are going to remove mirrored project
$name ($url) from the site.</p>
<form method="post">
<input type="hidden" name="name" value="$name" />
EOT
if ($Girocco::Config::project_passwords) {
	print <<EOT;
<p>Admin password: <input type="password" name="cpwd" /> <sup><a href="pwproj.cgi?name=$name">(forgot password?)</a></sup></p>
EOT
}
print <<EOT;
<p><input type="submit" name="y0" value="Remove" /></p>
</form>
EOT
